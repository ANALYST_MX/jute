#include "Value.hxx"

namespace jute {

  std::string Value::deserialize(const std::string &ref)
  {
    std::string out = "";
    for (auto i = 0; i < ref.length(); ++i)
      {
        if ('\\' == ref[i] && i + 1 < ref.length())
          {
            int plus = 2;

            switch (ref[i + 1]) {
              case '\"':
                out += '"';
                break;
              case '\\':
                out += '\\';
                break;
              case '/':
                out += '/';
                break;
              case 'b':
                out += '\b';
                break;
              case 'f':
                out += '\f';
                break;
              case 'n':
                out += '\n';
                break;
              case 'r':
                out += '\r';
                break;
              case 't':
                out += '\t';
                break;
              case 'u':
                if (i + 5 < ref.length())
                  {
                    unsigned long long v = 0;
                    for (auto j = 0; j < 4; ++j)
                      {
                        v *= 16;
                        if (ref[i + 2 + j] <= '9' && ref[i + 2 + j] >= '0')
                          {
                            v += ref[i + 2 + j] - '0';
                          }
                        if (ref[i + 2 + j] <= 'f' && ref[i + 2 + j] >= 'a')
                          {
                            v += ref[i + 2 + j] - 'a' + 10;
                          }
                      }
                    out += static_cast<char>(v);
                    plus = 6;
                  }
                break;
              }

            i += plus - 1;
            continue;
          }

        out += ref[i];
      }
    return out;
  }

  std::string Value::makesp(int d)
  {
    std::string s = "";
    while (d--)
      {
        s.append("  ");
      }

    return s;
  }

  std::string Value::convert(int d)
  {
    std::string res = "";

    switch (type)
      {
      case JSTRING:
        res = "\"" + value + "\"";
        break;
      case JNUMBER:
      case JBOOLEAN:
        res = value;
        break;
      case JOBJECT:
        res = "{\n";
        for (auto i = 0; i < properties.size(); ++i)
          {
            res += makesp(d) + "\"" + properties[i].first + "\": " + properties[i].second->convert(d + 1) + std::string(i == properties.size() - 1 ? "" : ",") + std::string("\n");
          }
        res += makesp(d - 1) + "}";
        break;
      case JARRAY:
        res = "[";
        for (auto i = 0; i < arr.size(); ++i)
          {
            if (0 != i)
              {
                res += ", ";
              }
            res += arr[i]->convert(d + 1);
          }
        res += "]";
        break;
      case JNONE:
        res = "null";
        break;
      default:
        res = "##";
        break;
      }

    return res;
  }

  Value::Value(Type tp)
    : type(tp)
  {
  }

  std::string Value::toString()
  {
    return convert();
  }

  Type Value::getType()
  {
    return type;
  }

  void Value::setType(Type tp)
  {
    type = tp;
  }

  void Value::addProperty(std::string key, Value *v)
  {
    mpindex[key] = properties.size();
    properties.push_back(make_pair(key, v));
  }

  void Value::addElement(Value *v)
  {
    arr.push_back(v);
  }

  void Value::setString(std::string s)
  {
    value = s;
  }

  int Value::asInt()
  {
    return std::stoi(value);
  }

  double Value::asDouble()
  {
    return std::stod(value);
  }

  bool Value::asBool()
  {
    if (value == "true")
      {
        return true;
      }

    return false;
  }

  void *Value::asNone()
  {
    return NULL;
  }

  std::string Value::asString()
  {
    return deserialize(value);
  }

  int Value::size()
  {
    int res;

    switch (type)
      {
      case JOBJECT:
        res = properties.size();
        break;
      case JARRAY:
        res = arr.size();
        break;
      default:
        res = 0;
        break;
      }

    return res;
  }

  Value *Value::get(int i)
  {
    Value *res;

    switch (type)
      {
      case JOBJECT:
        res = properties[i].second;
        break;
      case JARRAY:
        res = arr[i];
        break;
      default:
        res = new Value();
        break;
      }

    return res;
  }

  Value *Value::get(std::string s)
  {
    if (mpindex.find(s) != mpindex.end())
      {
        return properties[mpindex[s]].second;
      }

    return new Value();
  }

  Value *Value::operator[](int i)
  {
    return get(i);
  }

  Value *Value::operator[](std::string s)
  {
    return get(s);
  }

} // namespace jute

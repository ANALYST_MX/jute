#include "main.hxx"

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);

  std::ifstream in("data.json");
  std::string str = "";
  std::string tmp;
  while (std::getline(in, tmp))
    {
      str += tmp;
    }
  jute::Value *v = jute::Parser::parse(str);
  std::cout << v->toString() << std::endl;
  // you can use jute::Parser::parseFile("data.json")
  std::cout << " ------ " << std::endl;
  std::cout << v->get("examples")->get(0)->get("attr")->get(0)->get("value")->asString() << std::endl;
  if (v->get("examples")->get(1)->get("mixed")->get(5)->get(1)->get(1)->asBool())
    {
      std::cout << v->get("examples")->get(1)->get("pie")->asDouble() << std::endl;
      std::cout << v->get("examples")->get(2)->toString() << std::endl;
    }

  return app.exec();
}


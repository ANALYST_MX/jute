#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <cstring>
#include <string>

#include "Type.hxx"

namespace jute {

class Value
{
public:
  Value(Type = JUNKNOWN);
  std::string toString();
  Type getType();
  void setType(Type);
  void addProperty(std::string, Value *);
  void addElement(Value *);
  void setString(std::string);
  int asInt();
  double asDouble();
  bool asBool();
  void *asNone();
  std::string asString();
  int size();
  Value *get(int);
  Value *get(std::string);
  Value *operator[](int);
  Value *operator[](std::string);

public:
  static std::string deserialize(const std::string &);

private:
  std::string makesp(int);
  std::string value;
  Type type;
  std::vector<std::pair<std::string, Value *> > properties;
  std::map<std::string, int> mpindex;
  std::vector<Value *> arr;
  std::string convert(int = 1);
};

} // namespace jute

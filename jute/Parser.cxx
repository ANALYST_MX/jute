#include "Parser.hxx"

namespace jute {

  struct Parser::token
  {
    std::string value;
    tokenType type;
    token(std::string value = "", tokenType type = UNKNOWN): value(value), type(type) {}
  };

  bool Parser::isWhitespace(const char c)
  {
    return isspace(c);
  }

  int Parser::nextWhitespace(const std::string &source, int i)
  {
    while (i < source.length())
      {
        if (source[i] == '"')
          {
            for (++i; i < source.length() && ('"' != source[i] || '\\' == source[i - 1]); ++i);
          }
        if (source[i] == '\'')
          {
            for (++i; i < source.length() && ('\'' != source[i] || '\\' == source[i - 1]); ++i);
          }
        if (isWhitespace(source[i]))
          {
            return i;
          }

        ++i;
      }

    return source.length();
  }

  int Parser::skipWhitespaces(const std::string &source, int i)
  {
    while (i < source.length())
      {
        if (!isWhitespace(source[i]))
          {
            return i;
          }

        ++i;
      }

    return -1;
  }

  std::vector<Parser::token> Parser::tokenize(std::string source)
  {
    source += " ";
    std::vector<token> tokens;
    int index = skipWhitespaces(source, 0);
    while (index >= 0)
      {
        int next = nextWhitespace(source, index);
        std::string str = source.substr(index, next - index);
        int k = 0;
        while (k < str.length())
          {
            switch (str[k])
              {
              case '"':
                {
                  int tmp_k = k + 1;
                  for (; tmp_k < str.length() && ('"' != str[tmp_k] || '\\' == str[tmp_k - 1]); ++tmp_k);
                  tokens.push_back(token(str.substr(k + 1, tmp_k - k - 1), STRING));
                  k = tmp_k + 1;
                  break;
                }
              case '\'':
                {
                  int tmp_k = k + 1;
                  for (; tmp_k < str.length() && ('\'' != str[tmp_k] || '\\' == str[tmp_k - 1]); ++tmp_k);
                  tokens.push_back(token(str.substr(k + 1, tmp_k - k - 1), STRING));
                  k = tmp_k + 1;
                  break;
                }
              case ',':
                tokens.push_back(token(",", COMMA));
                ++k;
                break;
              case 't':
                if (k + 3 < str.length() && str.substr(k, 4) == "true")
                  {
                    tokens.push_back(token("true", BOOLEAN));
                    k += 4;
                  }
                break;
              case 'f':
                if (k + 4 < str.length() && str.substr(k, 5) == "false")
                  {
                    tokens.push_back(token("false", BOOLEAN));
                    k += 5;
                  }
                break;
              case 'n':
                if (k + 3 < str.length() && str.substr(k, 4) == "null")
                  {
                    tokens.push_back(token("null", NONE));
                    k += 4;
                  }
                break;
              case '}':
                tokens.push_back(token("}", CROUSH_CLOSE));
                ++k;
                break;
              case '{':
                tokens.push_back(token("{", CROUSH_OPEN));
                ++k;
                break;
              case ']':
                tokens.push_back(token("]", BRACKET_CLOSE));
                ++k;
                break;
              case '[':
                tokens.push_back(token("[", BRACKET_OPEN));
                ++k;
                break;
              case ':':
                tokens.push_back(token(":", COLON));
                ++k;
                break;
              case '-':
              case '0':
              case '1':
              case '2':
              case '3':
              case '4':
              case '5':
              case '6':
              case '7':
              case '8':
              case '9':
                {
                  int tmp_k = k;
                  if ('-' == str[tmp_k])
                    {
                      ++tmp_k;
                    }
                  for (; tmp_k < str.size() && ((str[tmp_k] <= '9' && str[tmp_k] >= '0') || '.' == str[tmp_k]); ++tmp_k);
                  tokens.push_back(token(str.substr(k, tmp_k - k), NUMBER));
                  k = tmp_k;
                  break;
                }
              default:
                tokens.push_back(token(str.substr(k), UNKNOWN));
                k = str.length();
                break;
              }
          }

        index = skipWhitespaces(source, next);
      }

    return tokens;
  }

  Value *Parser::jsonParse(std::vector<token> v, int i, int &r)
  {
    Value *current = new Value;
    switch (v[i].type) {
      case CROUSH_OPEN:
        {
          current->setType(JOBJECT);
          int k = i + 1;
          while (CROUSH_CLOSE != v[k].type)
            {
              std::string key = v[k].value;
              k += 2; // k+1 should be ':'
              int j = k;
              Value *vv = jsonParse(v, k, j);
              current->addProperty(key, vv);
              k = j;
              if (COMMA == v[k].type)
                {
                  k++;
                }
            }
          r = k + 1;
          break;
        }
      case BRACKET_OPEN:
        {
          current->setType(JARRAY);
          int k = i + 1;
          while (BRACKET_CLOSE != v[k].type)
            {
              int j = k;
              Value *vv = jsonParse(v, k, j);
              current->addElement(vv);
              k = j;
              if (COMMA == v[k].type)
                {
                  ++k;
                }
            }
          r = k + 1;
          break;
        }
      case NUMBER:
        current->setType(JNUMBER);
        current->setString(v[i].value);
        r = i + 1;
        break;
      case STRING:
        current->setType(JSTRING);
        current->setString(v[i].value);
        r = i + 1;
        break;
      case BOOLEAN:
        current->setType(JBOOLEAN);
        current->setString(v[i].value);
        r = i + 1;
        break;
      case NONE:
        current->setType(JNONE);
        current->setString("null");
        r = i + 1;
        break;
      default:
        break;
      }

    return current;
  }

  Value *Parser::parse(const std::string &str)
  {
    int k;
    return jsonParse(tokenize(str), 0, k);
  }

  Value *Parser::parseFile(const std::string &filename)
  {
    std::ifstream read(filename.c_str());
    std::string str = "";
    std::string tmp;
    while (std::getline(read, tmp))
      {
        str.append(tmp);
      }
    read.close();

    return Parser::parse(str);
  }

} // namespace jute

#pragma once

namespace jute {

  enum Type {JOBJECT, JSTRING, JARRAY, JBOOLEAN, JNUMBER, JNONE, JUNKNOWN};

} // namespace jute

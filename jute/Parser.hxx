#pragma once

#include <string>
#include <vector>
#include <cctype>

#include "Value.hxx"

namespace jute {

class Parser
{
public:
  static Value *parse(const std::string &);
  static Value *parseFile(const std::string &);

private:
  enum tokenType {UNKNOWN, STRING, NUMBER, CROUSH_OPEN, CROUSH_CLOSE, BRACKET_OPEN, BRACKET_CLOSE, COMMA, COLON, BOOLEAN, NONE};
  struct token;
  static bool isWhitespace(const char);
  static int nextWhitespace(const std::string &, int);
  static int skipWhitespaces(const std::string &, int);
  static std::vector<token> tokenize(std::string);
  static Value *jsonParse(std::vector<token>, int, int &);
};

} // namespace jute

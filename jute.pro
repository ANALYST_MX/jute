QT += core
QT -= gui

TARGET = jute
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cxx \
    jute/Value.cxx \
    jute/Parser.cxx

HEADERS += \
    main.hxx \
    jute/Value.hxx \
    jute/Type.hxx \
    jute/Parser.hxx \
    jute/Jute.hxx

CONFIG += c++11
